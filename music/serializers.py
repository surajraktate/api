from rest_framework import serializers
from .models import Employee, Songs


class EmployeeSerializer(serializers.ModelSerializer):
    #status = StatusSerializer(many=True)
    class Meta:
        model = Employee
        #fields = ['id', 'name', 'age']
        #To Every Thing
        fields = ('__all__')



class SongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Songs
        #To Every Thing from database
        fields = ('__all__')