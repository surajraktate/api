from django.core.handlers import exception
from django.http import Http404, HttpResponseBadRequest
from .models import Employee, Songs
from .serializers import EmployeeSerializer, SongSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics, status, request

from django.shortcuts import get_object_or_404


class EmployeeList(APIView):

    def get(self, request):
        # id = self.kwargs['id']
        # print("\n\n\n\n\n"+str(id)+"\n\n\n\n\n")

        id = int(request.GET.get('id', '0'))
        if int(id) == 0:
            employeeObj = Employee.objects.all().order_by('id')
        else:
            employeeObj = Employee.objects.all().filter(id=id)
            if len(employeeObj) == 0:
                newdict = {'Status': {"code": 404, "type": "Failed", "message": "Failed"}, 'result': {}}
                return Response(newdict)

        serializer = EmployeeSerializer(employeeObj, many=True)

        newdict = {'Status': {"code": 200, "type": "success", "message": "ok"}, 'result': serializer.data}

        return Response(newdict)

    def post(self, request):
        serializer = EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        employee = self.get_object(pk)
        serializer = EmployeeSerializer(employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        employee = self.get_object(pk)
        employee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


#
# class EmployeeList(generics.ListAPIView):
#
#     serializer_class = EmployeeSerializer
#
#     def get_queryset(self):
#         print("\n\n\n\n\n"+request.query_params.get('id')+"\n\n\n\n\n")
#
# #
# class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Employee.objects.all()
#     serializer_class = EmployeeSerializer


class SongsList(generics.ListAPIView):
    serializer_class = SongSerializer
    queryset = Songs.objects.all()

    def get_queryset(self):
        pk = self.request.query_params.get('pk', None)
        print(self.request.data)
        if pk != None and pk != '':
            try:
                if len(self.queryset.all().filter(pk=int(pk))) != 0:
                    return self.queryset.all().filter(pk=int(pk))
                else:
                    raise Http404
            except:
                raise Http404
        else:
            return self.queryset.all()


class SongsDelete(generics.DestroyAPIView):
    serializer_class = SongSerializer
    queryset = Songs.objects.all()


# class SongsDetails(generics.RetrieveUpdateDestroyAPIView):
#     # lookup_field = "pk"
#     serializer_class = SongSerializer(many=True)
#     queryset = Songs.objects.all()
#
#     # def get_object(self):
#     #     queryset = self.filter_queryset(self.get_queryset())
#     #     # make sure to catch 404's below
#     #
#     #     obj = ""
#     #     try:
#     #
#     #         obj = queryset.get(pk=self.request.user.pk)
#     #         self.check_object_permissions(self.request, obj)
#     #     except:
#     #         obj = queryset.get()
#     #     return obj
#     #
#     def get_object(self):
#         queryset = self.get_queryset()
#         filter = {}
#         for field in self.multiple_lookup_fields:
#             filter[field] = self.kwargs[field]
#
#         obj = get_object_or_404(queryset, **filter)
#         self.check_object_permissions(self.request, obj)
#         return obj
#
