from django.conf.urls import url

from music import views

urlpatterns = [

    url(r'^$', views.EmployeeList.as_view(), name="Employee"),
    url(r'^(P<pk>\d+)/$', views.EmployeeList.as_view(), name="EmployeePID"),
    #url(r'^song/', views.SongsAll.as_view(), name="Songs"),
    # url(r'^songall/', views.SongsAll.as_view(), name="Songs")

]
